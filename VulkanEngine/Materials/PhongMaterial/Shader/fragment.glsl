#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 2) uniform LightBuffer{
	vec4 pos;
	vec4 color;
} light;

layout(location = 0) in vec3 fragColor;
layout(location = 2) in vec3 pos;
layout(location = 3) in vec3 normal;
layout(location = 4) in vec3 camera;

layout(location = 0) out vec4 outColor;

void main(){

	vec3 vLight = normalize(pos - light.pos.xyz);
	float tetaLight = dot(-vLight, normal);

	vec3 vOrigine = normalize(camera - pos);

	vec3 vReflect = normalize(reflect(vLight, normal));
	float tetaVue = dot(vReflect, vOrigine);

	vec3 diffus = light.color.xyz * max(tetaLight, 0.0);
	vec3 specular = vec3(1.0) * pow(max(tetaVue, 0.0), 256);

	outColor = vec4(diffus * 0.5 + specular * 0.5, 1.0);

	//outColor = vec4(light.color.xyz, 1.0);
	
}