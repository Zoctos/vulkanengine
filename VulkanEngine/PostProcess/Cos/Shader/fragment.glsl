#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(set=0, input_attachment_index = 0, binding=0) uniform subpassInput imageColor;

layout(location = 0) in vec2 uv;

layout(location = 0) out vec4 outColor;

void main(){

	vec3 c = subpassLoad(imageColor).rgb;
	
	//vec3 fColor = c + vec3(1.0f, 0.0f, 0.0f);
	vec3 fColor = vec3(1.0f, 0.0f, 1.0f);
	
	outColor = vec4(fColor, 1.0f);
	
}