
//#include "Application\Application.h"
#include "Engine\Core\App.h"

#include <iostream>

int main() {

	//Application app;
	App app;

	try {
		app.camera = new Camera(glm::vec3(3.0f, 0.0f, 3.0f), glm::vec3(0.0f), glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		app.run();
	}
	catch (const std::runtime_error &e) {
		std::cerr << e.what() << std::endl;
		system("pause");
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;

}