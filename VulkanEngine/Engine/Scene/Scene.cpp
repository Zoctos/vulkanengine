#include "Scene.h"



Scene::Scene()
{
	//camera = new Camera(glm::vec3(3.0f, 0.0f, 3.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	camera = new Camera(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f));

	light = {};
	light.pos = glm::vec4(0.0f, 10.0f, 0.0f, 0.0f);
	light.color = glm::vec4(1.0f, 0.0f, 0.5f, 0.0f);

	Material *textureMaterial = new Material("TextureMaterial");
	textureMaterial->textureName = "chalet.jpg";
	Mesh *m = new Mesh("chalet.obj", textureMaterial);
	listMesh.push_back(m);

	//Material *colorMaterial = new Material("ColorMaterial");
	Material *colorMaterial = new Material("PhongMaterial"); //Material en cours de realisation (manque les normals)
	colorMaterial->textureName = "chalet.jpg";
	Mesh *m2 = new Mesh("chalet.obj", colorMaterial);
	m2->translate(glm::vec3(3.0, 0.0, 0.0));
	listMesh.push_back(m2);

	post = nullptr;

}


Scene::~Scene()
{
	for (int i = 0; i < listMesh.size(); ++i) {
		delete listMesh[i];
	}

}

void Scene::init(Device *device, SwapChain *swapChain, RenderPass *renderPass, Framebuffer *framebuffer, VkCommandPool commandPool, VkQueue graphics) {

	for (int i = 0; i < listMesh.capacity(); i++) {
		listMesh[i]->initPipelineGraphic(device, swapChain, renderPass, commandPool, graphics);
	}

	//post = new PostProcess("Cos", device, swapChain, renderPass, graphics, commandPool, camera);

	if (commandBuffers.size() > 0) {
		vkFreeCommandBuffers(device->getLogicalDevice(), commandPool, commandBuffers.size(), commandBuffers.data());
	}

	commandBuffers.resize(framebuffer->getFramebuffers().size());

	//Param�trage des command buffer
	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = commandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = (uint32_t)commandBuffers.size();

	//On cr�e les command buffers
	if (vkAllocateCommandBuffers(device->getLogicalDevice(), &allocInfo, commandBuffers.data()) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate command buffers !");
	}

	//Pour chaque command buffer cr��
	for (int i = 0; i < commandBuffers.size(); i++) {

		//On pr�pare les informations concernant l'enregistrement des donn�es du buffer
		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

		//On lance l'enregistrement du command buffer
		vkBeginCommandBuffer(commandBuffers[i], &beginInfo);

			//On param�tre le render pass
			VkRenderPassBeginInfo renderPassInfo = {};
			renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			renderPassInfo.renderPass = renderPass->getRenderPass();
			renderPassInfo.framebuffer = framebuffer->getFramebuffers().at(i);
			renderPassInfo.renderArea.offset = { 0, 0 };
			renderPassInfo.renderArea.extent = swapChain->getSwapChainExtent();

			std::array<VkClearValue, 2> clearValues = {};
			clearValues[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
			clearValues[1].depthStencil = { 1.0f, 0 };

			renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
			renderPassInfo.pClearValues = clearValues.data();

			//On lance le render pass
			vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
				
			for (int e = 0; e < listMesh.size(); ++e) {

				//On lie le pipeline graphique
				vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, listMesh[e]->getMaterial()->getGraphicPipeline()->getGraphicPipeline());

				//On lie les vertex buffers au command buffer
				VkBuffer vertexBuffers[] = { listMesh[e]->getMaterial()->getGraphicPipeline()->getVertexBuffer() };
				VkDeviceSize offsets[] = { 0 };
				vkCmdBindVertexBuffers(commandBuffers[i], 0, 1, vertexBuffers, offsets);

				VkBuffer indexBuffer = listMesh[e]->getMaterial()->getGraphicPipeline()->getIndexBuffer();

				//On lie le buffer des index au command buffer
				vkCmdBindIndexBuffer(commandBuffers[i], indexBuffer, 0, VK_INDEX_TYPE_UINT32);

				VkDescriptorSet ds = listMesh[e]->getMaterial()->getGraphicPipeline()->getDescriptorSet();

				//On lie le descriptor set avec le shader
				vkCmdBindDescriptorSets(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, listMesh[e]->getMaterial()->getGraphicPipeline()->getPipelineLayout(), 0, 1, &ds, 0, nullptr);

				//Affiche le r�sultat du command buffer
				vkCmdDrawIndexed(commandBuffers[i], static_cast<uint32_t>(listMesh[e]->getIndices().size()), 1, 0, 0, 0);

			}

			//On termine le render pass
			
			vkCmdEndRenderPass(commandBuffers[i]);

			/*vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
			

			vkCmdNextSubpass(commandBuffers[i], VK_SUBPASS_CONTENTS_INLINE);

				vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, post->getGraphicPipeline()->getGraphicPipeline());
				
				VkDescriptorSet ds = post->getGraphicPipeline()->getDescriptorSet();

				//On lie le descriptor set avec le shader
				vkCmdBindDescriptorSets(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, post->getGraphicPipeline()->getPipelineLayout(), 0, 1, &ds, 0, nullptr);

				//On lie les vertex buffers au command buffer
				VkBuffer vertexBuffers[] = { post->getGraphicPipeline()->getVertexBuffer() };
				VkDeviceSize offsets[] = { 0 };
				vkCmdBindVertexBuffers(commandBuffers[i], 0, 1, vertexBuffers, offsets);

				VkBuffer indexBuffer = post->getGraphicPipeline()->getIndexBuffer();

				//On lie le buffer des index au command buffer
				vkCmdBindIndexBuffer(commandBuffers[i], indexBuffer, 0, VK_INDEX_TYPE_UINT32);

				//Affiche le r�sultat du command buffer
				//vkCmdDrawIndexed(commandBuffers[i], static_cast<uint32_t>(post->getIndices().size()), 1, 0, 0, 0);
				vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

			vkCmdEndRenderPass(commandBuffers[i]);*/

		//On termine l'enregistrement dans le buffer et on v�rifie que tout c'est bien pass�
		if (vkEndCommandBuffer(commandBuffers[i]) != VK_SUCCESS) {
			throw std::runtime_error("failed to record command buffer !");
		}

	}

}

VkResult Scene::render(Device *device, SwapChain *swapChain, RenderPass *renderPass, VkCommandPool commandPool, VkQueue graphicsQueue, VkQueue presentQueue, VkSemaphore imageAvailableSemaphore, VkSemaphore renderFinishedSemaphore, uint32_t imageIndex) {

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

	//On indique la s�maphore � attendre
	VkSemaphore waitSemaphores[] = { imageAvailableSemaphore };
	VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = waitSemaphores;
	submitInfo.pWaitDstStageMask = waitStages;

	//On sp�cifie le command buffer concern�
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffers[imageIndex];

	//On pr�pare la s�maphore qui nous indiquera quand l'ex�cution du command buffer sera termin�
	VkSemaphore signalSemaphores[] = { renderFinishedSemaphore };
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = signalSemaphores;

	//On envoie le command buffer vers le graphicsqueue
	if (vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE) != VK_SUCCESS) {
		throw std::runtime_error("failed to submit draw command buffer !");
	}

	//On configure la pr�sentation pour l'image de la frame (en indiquant que l'on doit attendre la s�maphore signalSemaphore)
	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = signalSemaphores;

	//On indique dans quel swap chain on doit r�cup�rer l'image
	VkSwapchainKHR swapChains[] = { swapChain->getSwapChain() };
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = swapChains;
	presentInfo.pImageIndices = &imageIndex;

	//On envoie l'image sur l'�cran
	return vkQueuePresentKHR(presentQueue, &presentInfo);

}

void Scene::update(Device *device, SwapChain *swapChain, float time) {

	//On applique la rotation sur le mod�le
	UniformBufferObject ubo = {};
	ubo.model = glm::rotate(glm::mat4(1.0f), time * glm::radians(0.0f), glm::vec3(1.0f, 1.0f, 1.0f));
	ubo.view = glm::lookAt(camera->pos, camera->pos + camera->cameraFront, camera->cameraUp);
	ubo.proj = glm::perspective(glm::radians(45.0f), swapChain->getSwapChainExtent().width / (float)swapChain->getSwapChainExtent().height, 0.1f, 10.0f);
	ubo.proj[1][1] *= -1; //les coordonnes en y sont inverses entre OpenGL et Vulkan
	ubo.camera = glm::vec4(camera->pos, 0.0f);

	//On copie les donn�es du UBO et de la lumiere dans le buffer
	for (int i = 0; i < listMesh.capacity(); i++) {
		void *data;
		vkMapMemory(device->getLogicalDevice(), listMesh[i]->getMaterial()->getGraphicPipeline()->getUniformBufferMemory(), 0, sizeof(ubo), 0, &data);
		memcpy(data, &ubo, sizeof(ubo));
		vkUnmapMemory(device->getLogicalDevice(), listMesh[i]->getMaterial()->getGraphicPipeline()->getUniformBufferMemory());

		void *dataLight;
		vkMapMemory(device->getLogicalDevice(), listMesh[i]->getMaterial()->getGraphicPipeline()->getLightBufferMemory(), 0, sizeof(light), 0, &dataLight);
		memcpy(dataLight, &light, sizeof(light));
		vkUnmapMemory(device->getLogicalDevice(), listMesh[i]->getMaterial()->getGraphicPipeline()->getLightBufferMemory());

	}

}

void Scene::cleanupScene(Device *device, VkCommandPool commandPool) {

	vkFreeCommandBuffers(device->getLogicalDevice(), commandPool, static_cast<uint32_t>(commandBuffers.size()), commandBuffers.data());

	for (int i = 0; i < listMesh.capacity(); ++i) {
		vkDestroyPipeline(device->getLogicalDevice(), listMesh[i]->getMaterial()->getGraphicPipeline()->getGraphicPipeline(), nullptr);
		vkDestroyPipelineLayout(device->getLogicalDevice(), listMesh[i]->getMaterial()->getGraphicPipeline()->getPipelineLayout(), nullptr);
	}
}

Camera* Scene::getCamera() {
	return this->camera;
}