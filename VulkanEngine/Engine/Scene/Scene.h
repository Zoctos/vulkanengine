#pragma once

#include <vector>

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

#include "../Core/Camera.h"
#include "../Core/Framebuffer.h"
#include "../Core/Mesh.h"
#include "../Core/PostProcess.h"

class Scene
{
public:
	Scene();
	~Scene();

	void init(Device *device, SwapChain *swapChain, RenderPass *renderPass, Framebuffer *framebuffer, VkCommandPool commandPool, VkQueue graphics);
	VkResult render(Device *device, SwapChain *swapChain, RenderPass *renderPass, VkCommandPool commandPool, VkQueue graphicsQueue, VkQueue presentQueue, VkSemaphore imageAvailableSemaphore, VkSemaphore renderFinishedSemaphore, uint32_t imageIndex);
	void update(Device *device, SwapChain *swapChain, float time);

	void cleanupScene(Device *device, VkCommandPool commandPool);

	Camera* getCamera();

private:
	std::vector<Mesh*> listMesh;
	Camera *camera;
	LightBuffer light;

	std::vector<VkCommandBuffer> commandBuffers;

	PostProcess *post;

};

