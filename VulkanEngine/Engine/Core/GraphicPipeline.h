#pragma once

#include "Buffer.h"
#include "Device.h"
#include "Shader.h"
#include "SwapChain.h"
#include "TextureImage.h"
#include "Vertice.h"
#include "RenderPass.h"
#include "../Tools.h"

#include <array>

class GraphicPipeline
{
public:
	GraphicPipeline(Device *device);
	~GraphicPipeline();

	void setVertexShader(std::vector<char> code);
	void setFragmentShader(std::vector<char> code);

	void createDescriptorSetLayout();
	void createGraphicPipeline(VkPipelineViewportStateCreateInfo viewportState, VkRenderPass renderPass);

	void createTextureImage(std::string textureName, VkCommandPool commandPool, VkQueue graphicsQueue);

	void createVertexBuffer(std::vector<Vertice> *vertices, VkQueue graphicsQueue, VkCommandPool commandPool);
	void createIndexBuffer(std::vector<uint32_t> indices, VkQueue graphicsQueue, VkCommandPool commandPool);
	void createUniformBuffer();
	
	void createDescriptorPool();
	void createDescriptorSet();

	VkDescriptorSet getDescriptorSet();
	VkBuffer getVertexBuffer();
	VkBuffer getIndexBuffer();
	VkDeviceMemory getUniformBufferMemory();
	VkPipeline getGraphicPipeline();
	VkPipelineLayout getPipelineLayout();

	VkDeviceMemory getLightBufferMemory();

	TextureImage* getTextureImage();

	int subpass;

private:
	Shader *vertexShader;
	Shader *geometryShader;
	Shader *fragmentShader;

	VkDescriptorPool descriptorPool;
	VkDescriptorSet descriptorSet;
	VkDescriptorSetLayout descriptorSetLayout;

	VkBuffer vertexBuffer;
	VkDeviceMemory vertexBufferMemory;
	VkBuffer indexBuffer;
	VkDeviceMemory indexBufferMemory;
	VkBuffer uniformBuffer;
	VkDeviceMemory uniformBufferMemory;

	VkBuffer lightBuffer;
	VkDeviceMemory lightBufferMemory;

	VkPipelineLayout pipelineLayout;
	VkPipeline graphicsPipeline;

	//-------
	TextureImage *tImage;
	Device *device;
	
	std::vector<VkDescriptorSetLayoutBinding> layoutBinding;
	std::vector<VkWriteDescriptorSet> descriptorWrites;

	void addDescriptorSetLayout(int binding, int descriptorCount, VkDescriptorType descriptorType, VkShaderStageFlagBits stageFlags, VkSampler *sampler);
	void addDescriptorWrite(int dstBinding, int dstArrayElement, VkDescriptorType descriptorType, int descriptorCount, VkDescriptorBufferInfo *pBuffer);
	void addDescriptorWriteImg(int dstBinding, int dstArrayElement, VkDescriptorType descriptorType, int descriptorCount, VkDescriptorImageInfo *pImage);

};

