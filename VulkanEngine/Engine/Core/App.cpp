#include "App.h"

void App::run() {

	//Cr�ation de la fen�tre
	initWindow();

	//Cr�ation de l'environnement Vulkan
	initVulkan();

	//On appel la boucle principale qui g�re le programme
	mainLoop();

	//On d�salloue tout les objets avant de fermer le programme
	cleanup();

}

/**
	Cr�er la fen�tre de l'application
*/
void App::initWindow() {

	//Initialise la biblioth�que GLFW
	glfwInit();

	//On indique que l'on ne va pas cr�er de context OpenGL
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

	//On cr�e la fen�tre
	window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan Engine", nullptr, nullptr);

	glfwSetWindowUserPointer(window, this);
	glfwSetWindowSizeCallback(window, App::onWindowResized);

}

void App::initVulkan() {
	
	//Initialisation de Vulkan
	createInstance();

	//Initialisation du debug callback
	setupDebugCallback();

	//Cr�ation de la surface de la fen�tre
	createSurface();

	//R�cup�ration de la carte graphique
	device = new Device();
	pickPhysicalDevice();
	//Initialisation du p�riph�rique logique
	createLogicalDevice();

	//Cr�ation de la swap chain
	swapChain = new SwapChain();
	createSwapChain();

	//Cr�ation des images views
	//createImageViews();
	swapChain->createSwapChainImageView(device);

	//Cr�ation du render pass
	renderPass = new RenderPass();
	createRenderPass();

	//Cr�ation du pipeline graphique
	//graphicPipeline = new GraphicPipeline(device);
	//createGraphicsPipeline();

	//Cr�ation du command pool
	createCommandPool();

	//Cr�ation du depth buffer
	createDepthRessources();

	//Cr�ation des framebuffers
	createFramebuffers();

	//Cr�ation des command buffers
	createCommandBuffers();

	scene = new Scene();
	scene->init(device, swapChain, renderPass, framebuffer, commandPool, graphicsQueue);

	//Cr�ation des s�maphores
	createSemaphores();
}

void App::createInstance() {

	if (enableValidationLayers && !checkValidationLayerSupport()) {
		throw std::runtime_error("validation layers requested, but not available");
	}

	//Permet de d�finir des informations sur l'application
	VkApplicationInfo appInfo = {};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = "Vulkan Engine";
	appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.pEngineName = "No Engine";
	appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.apiVersion = VK_API_VERSION_1_0;

	instance = new Instance();
	instance->initInstance(appInfo, getRequiredExtensions(), enableValidationLayers, validationLayers);

}

/**
Permet d'initialiser le debug callback si besoin
*/
void App::setupDebugCallback() {

	//Si on n'a pas activ� les validation layer on n'a pas besoin de mettre en place le debug callback
	if (!enableValidationLayers)
		return;

	//On pr�pare le debug callback
	VkDebugReportCallbackCreateInfoEXT createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
	createInfo.flags = VK_DEBUG_REPORT_DEBUG_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
	createInfo.pfnCallback = debugCallBack;

	//Et on le cr�e
	if (LayersManagement::createDebugReportCallbackEXT(instance->getInstanceVk(), &createInfo, nullptr, &callback) != VK_SUCCESS) {
		throw std::runtime_error("failed to set up debug callback");
	}
}

/**
Permet de cr�er la surface de la fen�tre
*/
void App::createSurface() {

	if (glfwCreateWindowSurface(instance->getInstanceVk(), window, nullptr, &surface) != VK_SUCCESS) {
		throw std::runtime_error("failed to create window surface");
	}
}

/**
	Permet de r�cup�rer la carte graphique qui sera utilis�
*/
void App::pickPhysicalDevice() {

	device->pickPhysicalDevice(instance->getInstanceVk(), surface);

}

/**
Permet de cr�er le device logique
*/
void App::createLogicalDevice() {

	device->createLogicalDevice(surface, &graphicsQueue, &presentQueue, enableValidationLayers, validationLayers);

}

/**
Cr�ation de la swap chain
*/
void App::createSwapChain() {

	swapChain->createSwapChain(device->querySwapChainSupport(surface), device->findQueueFamilies(surface), surface, window, device->getLogicalDevice());

}

/**
Cr�ation du render pass
*/
void App::createRenderPass() {

	renderPass->createRenderPass(device, swapChain->getSwapChainImageFormat());

}

void App::createDepthRessources() {

	VkFormat depthFormat = TextureImage::findDepthFormat(device);

	textureDepth = new TextureImage(device, swapChain->getSwapChainExtent().width, swapChain->getSwapChainExtent().height, depthFormat, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
		VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	textureDepth->createTextureImageView(device, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT);

	Buffer::transitionImageLayout(device, textureDepth->getImage(), depthFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, commandPool, graphicsQueue);

}

/**
	Cr�ation des framebuffers
*/
void App::createFramebuffers() {

	framebuffer = new Framebuffer();
	framebuffer->createFramebuffers(device, swapChain, renderPass->getRenderPass(), textureDepth);

}

void App::createCommandPool() {

	//On r�cup�re les queue families
	QueueFamilyIndices queueFamilyIndices = device->findQueueFamilies(surface);

	//On indique que notre command pool concerne les commandes d'affichage
	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily;

	//On cr�e le command pool
	if (vkCreateCommandPool(device->getLogicalDevice(), &poolInfo, nullptr, &commandPool) != VK_SUCCESS) {
		throw std::runtime_error("failed to create command pool !");
	}

}

/**
Cr�e les commandes buffers
*/
void App::createCommandBuffers() {
	
}

/**
Permet de cr�er les s�maphores pour le rendu et la pr�sentation
*/
void App::createSemaphores() {

	//On initialise les informations pour les deux s�maphores
	VkSemaphoreCreateInfo semaphoreInfo = {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	//Puis on essaye de les cr�er
	if (vkCreateSemaphore(device->getLogicalDevice(), &semaphoreInfo, nullptr, &imageAvailableSemaphore) != VK_SUCCESS ||
		vkCreateSemaphore(device->getLogicalDevice(), &semaphoreInfo, nullptr, &renderFinishedSemaphore) != VK_SUCCESS) {
		throw std::runtime_error("failed to create semaphores ! ");
	}

}

//---------------------------------------------------------

void App::recreateSwapChain() {

	vkDeviceWaitIdle(device->getLogicalDevice());

	cleanupSwapChain();

	createSwapChain();
	//createImageViews();
	createRenderPass();
	scene->init(device, swapChain, renderPass, framebuffer, commandPool, graphicsQueue);
	createFramebuffers();
	createCommandBuffers();

}

/**
	Permet de v�rifier si les layers que l'on souhaite sont support�s
*/
bool App::checkValidationLayerSupport() {

	//On r�cup�re la liste des layers
	uint32_t layerCount;
	vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

	std::vector<VkLayerProperties> availableLayers(layerCount);
	vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

	//On v�rifie que tout les layers que l'on veut existent
	for (const char * layerName : validationLayers) {
		bool layerFound = false;

		for (const auto &layerProperties : availableLayers) {

			if (strcmp(layerName, layerProperties.layerName) == 0) {
				layerFound = true;
				break;
			}

		}

		if (!layerFound)
			return false;
	}

	return true;

}

/**
Permet de r�cup�rer les extensions disponibles
*/
std::vector<const char*> App::getRequiredExtensions() {

	std::vector<const char*> extensions;

	unsigned int glfwExtensionCount = 0;
	const char** glfwExtensions;
	//On r�cup�re les extensions requises
	glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

	for (unsigned int i = 0; i < glfwExtensionCount; i++) {
		extensions.push_back(glfwExtensions[i]);
	}

	//Si les validations layers sont activ�s on ajoute un layer
	if (enableValidationLayers) {
		extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
	}

	return extensions;

}

/**
	Boucle principale du programme
*/
void App::mainLoop() {

	//Tant que la fen�tre n'est pas ferm�
	while (!glfwWindowShouldClose(window)) {

		//On lance la verification des input
		glfwPollEvents();

		int leftKey = glfwGetKey(window, GLFW_KEY_LEFT);
		int rightKey = glfwGetKey(window, GLFW_KEY_RIGHT);
		int upKey = glfwGetKey(window, GLFW_KEY_UP);
		int downKey = glfwGetKey(window, GLFW_KEY_DOWN);

		if (upKey == GLFW_PRESS) {
			//scene->camera->pos.x = scene->camera->pos.x + 0.01f;
			scene->getCamera()->pos += scene->getCamera()->cameraFront*0.01f;
			scene->getCamera()->lookAt.x = scene->getCamera()->lookAt.x + 0.01f;
		}
		else if (downKey == GLFW_PRESS) {
			//scene->camera->pos.x = scene->camera->pos.x - 0.01f;
			scene->getCamera()->pos -= scene->getCamera()->cameraFront*0.01f;
			scene->getCamera()->lookAt.x = scene->getCamera()->lookAt.x - 0.01f;
		}

		if(leftKey == GLFW_PRESS) {
			//scene->camera->pos.y = scene->camera->pos.y + 0.01f;
			scene->getCamera()->pos -= glm::normalize(glm::cross(scene->getCamera()->cameraFront, scene->getCamera()->cameraUp))*0.01f;
			scene->getCamera()->lookAt.y = scene->getCamera()->lookAt.y + 0.01f;
		}
		else if (rightKey == GLFW_PRESS) {
			//scene->camera->pos.y = scene->camera->pos.y - 0.01f;
			scene->getCamera()->pos += glm::normalize(glm::cross(scene->getCamera()->cameraFront, scene->getCamera()->cameraUp))*0.01f;
			scene->getCamera()->lookAt.y = scene->getCamera()->lookAt.y + 0.01f;
		}

		int mouseRight = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT);

		if (mouseRight == GLFW_PRESS) {
			double xpos, ypos;

			glfwGetCursorPos(window, &xpos, &ypos);

			scene->getCamera()->defineCameraOrientation(xpos, ypos);
		}
		else if (!scene->getCamera()->isFirstOrientation && mouseRight == GLFW_RELEASE) {
			
			scene->getCamera()->isFirstOrientation = true;

		}

		updateUniformBuffer();
		drawFrame();
	}

	vkDeviceWaitIdle(device->getLogicalDevice());

	//On detruit la fen�tre
	glfwDestroyWindow(window);

	//Et on indique que l'on arr�te la biblioth�que
	glfwTerminate();

}

/**
Permet de mettre � jour le buffer d'uniform (UBO)
*/
void App::updateUniformBuffer() {

	//On r�cup�re le temps de d�part
	static auto startTime = std::chrono::high_resolution_clock::now();

	//On r�cup�re le temps actuel
	auto currentTime = std::chrono::high_resolution_clock::now();

	//On calcule le temps qui s'est �coul� entre le d�part et maintenant
	float time = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count() / 1000.0f;

	scene->update(device, swapChain, time);

}

/**
Permet d'afficher une frame
*/
void App::drawFrame() {

	uint32_t imageIndex;

	//On r�cup�re l'image que l'on va afficher
	VkResult result = vkAcquireNextImageKHR(device->getLogicalDevice(), swapChain->getSwapChain(), std::numeric_limits<uint64_t>::max(), imageAvailableSemaphore, VK_NULL_HANDLE, &imageIndex);

	//Si on a une erreur de type out of date on recr�e la swap chain
	if (result == VK_ERROR_OUT_OF_DATE_KHR) {
		recreateSwapChain();
		return;
	}
	else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
		throw std::runtime_error("failed to acquire swap chain image");
	}

	result = scene->render(device, swapChain, renderPass, commandPool, graphicsQueue, presentQueue, imageAvailableSemaphore, renderFinishedSemaphore, imageIndex);

	if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR) {
		recreateSwapChain();
	}
	else if (result != VK_SUCCESS) {
		throw std::runtime_error("failed to present swap chain image !");
	}

}

/**
	Permet de d�sallouer les objets de la swap chain
*/
void App::cleanupSwapChain() {

	textureDepth->cleanTextureImage(device->getLogicalDevice());
	framebuffer->cleanup(device);

	scene->cleanupScene(device, commandPool);
	
	renderPass->cleanRenderPass(device->getLogicalDevice());

	for (size_t i = 0; i < swapChainImageViews.size(); i++) {
		vkDestroyImageView(device->getLogicalDevice(), swapChainImageViews[i], nullptr);
	}

	swapChain->cleanSwapChain(device->getLogicalDevice());

}

/**
	Permet de d�sallouer tout les objets du programme
*/
void App::cleanup() {
	
	cleanupSwapChain();

	delete scene;
	
	vkDestroySemaphore(device->getLogicalDevice(), renderFinishedSemaphore, nullptr);
	vkDestroySemaphore(device->getLogicalDevice(), imageAvailableSemaphore, nullptr);

	vkDestroyCommandPool(device->getLogicalDevice(), commandPool, nullptr);

	device->cleanLogicalDevice();

	LayersManagement::destroyDebugReportCallbackEXT(instance->getInstanceVk(), callback, nullptr);

	vkDestroySurfaceKHR(instance->getInstanceVk(), surface, nullptr);

	delete instance;

	glfwDestroyWindow(window);

	glfwTerminate();

}

/**
Appel� lorsque la fen�tre est redimensionn�
*/
void App::onWindowResized(GLFWwindow *window, int width, int height) {

	if (width == 0 || height == 0)
		return;

	App *app = reinterpret_cast<App*>(glfwGetWindowUserPointer(window));
	app->recreateSwapChain();

}