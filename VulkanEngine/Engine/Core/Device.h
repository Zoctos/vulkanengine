#pragma once

#include <vulkan\vulkan.h>

#include <iostream>
#include <set>
#include <vector>

/**
Utilis� pour v�rifier si la carte graphique supporte certaines QueueFamilies
*/
struct QueueFamilyIndices {
	int graphicsFamily = -1;
	int presentFamily = -1;

	//On regarde si la carte peut traiter les images et les passer � la surface
	bool isComplete() {
		return graphicsFamily >= 0 && presentFamily >= 0;
	}
};

/**
Utilis� pour conna�tre les propri�t�s de support de la swapchain par la surface
*/
struct SwapChainSupportDetails {

	//Propri�t�s basique de la surface (nombre d'images dans la swap chain, taille et hauteur, etc ..)
	VkSurfaceCapabilitiesKHR capabilities;
	//Pixel format, color space
	std::vector<VkSurfaceFormatKHR> formats;
	//Modes de pr�sentations
	std::vector<VkPresentModeKHR> presentModes;

};

class Device
{
public:
	Device();
	~Device();

	void pickPhysicalDevice(VkInstance instance, VkSurfaceKHR surface);
	void createLogicalDevice(VkSurfaceKHR surface, VkQueue *graphicsQueue, VkQueue *presentQueue, bool enableValidationLayers, std::vector<const char*> validationLayers);
	
	bool isDeviceSuitable(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface);
	QueueFamilyIndices findQueueFamilies(VkSurfaceKHR surface);
	QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR surface);
	bool checkDeviceExtensionSupport(VkPhysicalDevice physicalDevice);
	SwapChainSupportDetails querySwapChainSupport(VkSurfaceKHR surface);
	SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device, VkSurfaceKHR surface);

	uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);

	void cleanLogicalDevice();

	VkPhysicalDevice getPhysicalDevice();
	VkDevice getLogicalDevice();

private:
	VkPhysicalDevice physicalDevice;
	VkDevice logicalDevice;

	//Liste des device extensions requis
	const std::vector<const char*> deviceExtensions = {
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
	};

};

