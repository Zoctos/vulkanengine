#include "Instance.h"



Instance::Instance()
{
}


Instance::~Instance()
{
	vkDestroyInstance(instance, nullptr);
}

void Instance::initInstance(VkApplicationInfo &appInfo, std::vector<const char*> extensions, bool enableValidationLayers, std::vector<const char*> validationLayers) {

	//Permet de d�finir les extensions et les validation layers que l'on souhaite avoir
	VkInstanceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;

	//R�cup�ration des extensions
	createInfo.enabledExtensionCount = extensions.size();
	createInfo.ppEnabledExtensionNames = extensions.data();

	//Si les validations layers sont actifs on indique que l'on veut les utiliser
	if (enableValidationLayers) {
		createInfo.enabledLayerCount = validationLayers.size();
		createInfo.ppEnabledLayerNames = validationLayers.data();
	}
	else {
		createInfo.enabledLayerCount = 0;
	}

	//On essaye de cr�er l'instance
	VkResult result = vkCreateInstance(&createInfo, nullptr, &instance);

	//Si on n'y arrive pas on renvoie une erreur
	if (result != VK_SUCCESS) {
		throw std::runtime_error("failed to create vulkan instance");
	}

}

VkInstance Instance::getInstanceVk() {
	return instance;
}