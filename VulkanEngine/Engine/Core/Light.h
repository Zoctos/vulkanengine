#pragma once

#include <glm\glm.hpp>

struct LightBuffer {
	glm::vec4 pos;
	glm::vec4 color;
};