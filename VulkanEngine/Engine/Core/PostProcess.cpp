#include "PostProcess.h"

PostProcess::PostProcess(std::string name, Device *device, SwapChain *swapChain, RenderPass *renderPass, VkQueue graphicsQueue, VkCommandPool commandPool, Camera *camera) {

	this->name = name;
	graphicPipeline = new GraphicPipeline(device);
	graphicPipeline->subpass = 1;

	graphicPipeline->createDescriptorSetLayout();

	graphicPipeline->setVertexShader(Tools::readFile("PostProcess/" + name + "/Shader/vertex.glsl"));
	graphicPipeline->setFragmentShader(Tools::readFile("PostProcess/" + name + "/Shader/fragment.glsl"));

	//On cr�e le viewport
	VkViewport viewport = {};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = (float)swapChain->getSwapChainExtent().width;
	viewport.height = (float)swapChain->getSwapChainExtent().height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	//On cr�e la vue qui affiche la sc�ne (d�coupe ce qui est en dehors du cadre)
	VkRect2D scissor = {};
	scissor.offset = { 0, 0 };
	scissor.extent = swapChain->getSwapChainExtent();

	//On sp�cifie qu'on utilise le viewport qu'on a cr�e auparavant et on l'associe au scissor
	VkPipelineViewportStateCreateInfo viewportState = {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.pViewports = &viewport;
	viewportState.scissorCount = 1;
	viewportState.pScissors = &scissor;

	graphicPipeline->createGraphicPipeline(viewportState, renderPass->getRenderPass());

	//Cr�ation des vertices et des indices
	
	Vertice v1; v1.pos = glm::vec3(10.0f, 10.0f, 1.0f);
	Vertice v2; v2.pos = glm::vec3(-10.0f, 10.0f, 1.0f);
	Vertice v3; v3.pos = glm::vec3(-10.0f, -10.0f, 1.0f);
	Vertice v4; v4.pos = glm::vec3(10.0f, -10.0f, 1.0f);

	vertices = { v1, v2, v3, v4 };
	indices = { 0, 1, 3, 1, 2, 3 };
	
	graphicPipeline->createVertexBuffer(&vertices, graphicsQueue, commandPool);
	graphicPipeline->createIndexBuffer(indices, graphicsQueue, commandPool);
	
	graphicPipeline->createUniformBuffer();

	graphicPipeline->createDescriptorPool();
	graphicPipeline->createDescriptorSet();

}

void PostProcess::render(Device *device, VkCommandBuffer commandBuffer, VkImageView view, VkCommandPool commandPool, VkQueue graphicsQueue) {

	//graphicPipeline->createDescriptorPool();
	//graphicPipeline->createDescriptorSet();

}

GraphicPipeline* PostProcess::getGraphicPipeline() {
	return graphicPipeline;
}

std::vector<uint32_t> PostProcess::getIndices() {
	return indices;
}