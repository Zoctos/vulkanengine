#pragma once
#define GLFW_INCLUDE_VULKAN
#include <GLFW\glfw3.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

#include <algorithm>
#include <chrono>
#include <set>

#include "../LayersManagement/LayersManagement.h"
#include "../Scene/Scene.h"
#include "Device.h"
#include "Instance.h"

//----------------------

class App
{

public:

	void run();

	static void onWindowResized(GLFWwindow *window, int width, int height);
	
	Camera *camera;

	const int WIDTH = 800;
	const int HEIGHT = 800;
	
private:
	void initWindow();
	void initVulkan();
	void mainLoop();

	void createInstance();
	void setupDebugCallback();
	void createSurface();
	void pickPhysicalDevice();
	void createLogicalDevice();
	void createSwapChain();
	void createRenderPass();
	void createCommandPool();
	void createDepthRessources();
	void createFramebuffers();
	void createCommandBuffers();
	void createSemaphores();

	void updateUniformBuffer();
	void drawFrame();

	void recreateSwapChain();

	bool checkValidationLayerSupport();
	std::vector<const char*> getRequiredExtensions();

	void cleanupSwapChain();
	void cleanup();

	//VARIABLES
	GLFWwindow *window;

	VkDebugReportCallbackEXT callback;
	VkSurfaceKHR surface;
	VkCommandPool commandPool;
	VkSemaphore imageAvailableSemaphore;
	VkSemaphore renderFinishedSemaphore;

	VkQueue graphicsQueue;
	VkQueue presentQueue;

	std::vector<VkImageView> swapChainImageViews;
	//std::vector<VkFramebuffer> swapChainFramebuffers;

	//std::vector<VkCommandBuffer> commandBuffers;

	//--------------------------------
	
	Instance *instance;
	//GraphicPipeline *graphicPipeline;
	Device *device;
	SwapChain *swapChain;
	RenderPass *renderPass;
	Scene *scene;
	Framebuffer *framebuffer;
	TextureImage *textureDepth;

	//------------------------------

	#ifdef NDEBUG
		const bool enableValidationLayers = false;
	#else
		const bool enableValidationLayers = true;
	#endif

	//Liste des validations layers requis
	const std::vector<const char*> validationLayers = {
		"VK_LAYER_LUNARG_standard_validation"
	};

	//OTHER

	/**
	Permet d'afficher un message provenant d'un validation layer
	*/
	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallBack(
		VkDebugReportFlagsEXT flags,
		VkDebugReportObjectTypeEXT objType,
		uint64_t obj,
		size_t location,
		int32_t code,
		const char *layerPrefix,
		const char *msg,
		void *userData
	) {

		std::cerr << "Validation layer : " << msg << std::endl;

		return VK_FALSE;

	};

	/*
	std::vector<uint32_t> indices = {
		0, 1, 2, 2, 3, 0,
		4, 5, 6, 6, 7, 4
	};*/

};

