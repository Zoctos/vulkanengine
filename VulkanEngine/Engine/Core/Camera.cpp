#include "Camera.h"

#include <iostream>

Camera::Camera(glm::vec3 pos, glm::vec3 lookAt, glm::vec3 cameraFront, glm::vec3 cameraUp)
{
	this->pos = pos;
	this->lookAt = lookAt;
	this->cameraFront = cameraFront;
	this->cameraUp = cameraUp;

	isFirstOrientation = true;
	lastX = 400;
	lastY = 400;
	//pitch = 80.0f;
	pitch = 0.0f;
	yaw = -80.0f;

	defineCameraOrientation(0, 0);
	isFirstOrientation = true;
}


Camera::~Camera()
{
}

void Camera::defineCameraOrientation(double xpos, double ypos) {

	if (isFirstOrientation) {
		lastX = xpos;
		lastY = ypos;
		isFirstOrientation = false;
	}

	float xOffset = xpos - lastX;
	float yOffset = lastY - ypos;
	lastX = xpos;
	lastY = ypos;

	float sensitivity = 0.05f;
	xOffset *= sensitivity;
	yOffset *= sensitivity;

	yaw += xOffset;
	pitch += yOffset;

	if (pitch > 89.0f)
		pitch = 89.0f;
	if (pitch < -89.0f)
		pitch = -89.0f;

	glm::vec3 front;
	
	front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = sin(glm::radians(pitch));
	front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));

	cameraFront = glm::normalize(front);

	//std::cout << yaw << " ; " << pitch << std::endl;

}

void Camera::update() {

	

}