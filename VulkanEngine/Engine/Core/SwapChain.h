#pragma once

#include <GLFW\glfw3.h>

#include <vulkan\vulkan.h>

#include <algorithm>
#include <vector>

#include "Device.h"

class SwapChain
{
public:
	SwapChain();
	~SwapChain();

	void createSwapChain(SwapChainSupportDetails swapChainSupport, QueueFamilyIndices indices, VkSurfaceKHR surface, GLFWwindow *window, VkDevice logicalDevice);
	void createSwapChainImageView(Device *device);

	VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR> &availableFormats);
	VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR> availablePresentModes);
	VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR &capabilities, GLFWwindow *window);

	void cleanSwapChain(VkDevice logicalDevice);

	VkSwapchainKHR getSwapChain();
	std::vector<VkImage> getSwapChainImages();
	std::vector<VkImageView> getSwapChainImageViews();
	VkFormat getSwapChainImageFormat();
	VkExtent2D getSwapChainExtent();

private:
	VkSwapchainKHR swapChain;

	std::vector<VkImage> swapChainImages;
	std::vector<VkImageView> swapChainImageViews;

	VkFormat swapChainImageFormat;
	VkExtent2D swapChainExtent;

};

