#include "Device.h"



Device::Device()
{

}


Device::~Device()
{
}

/**
	Permet de r�cup�rer la carte graphique qui sera utilis�
*/
void Device::pickPhysicalDevice(VkInstance instance, VkSurfaceKHR surface) {

	//On r�cup�re la liste des cartes disponibles
	uint32_t deviceCount = 0;
	vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);

	//Si on en trouve aucune
	if (deviceCount == 0) {
		throw std::runtime_error("failed to find GPUs with Vulkan support !");
	}

	std::vector<VkPhysicalDevice> devices(deviceCount);
	vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());

	//On parcourt toutes les cartes et on r�cup�re la premi�re qui correspond � ce que l'on souhaite faire
	for (const auto &device : devices) {
		if (isDeviceSuitable(device, surface)) {
			physicalDevice = device;
			break;
		}
	}

	//Si on n'a rien trouv� on renvoie une erreur
	if (physicalDevice == VK_NULL_HANDLE) {
		throw std::runtime_error("failed to find a suitable GPU");
	}

}

/**
Permet de cr�er le device logique
*/
void Device::createLogicalDevice(VkSurfaceKHR surface, VkQueue *graphicsQueue, VkQueue *presentQueue, bool enableValidationLayers, std::vector<const char*> validationLayers) {

	//R�cup�ration des queue families qui nous int�resse
	QueueFamilyIndices indices = findQueueFamilies(surface);

	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
	std::set<int> uniqueQueueFamilies = { indices.graphicsFamily, indices.presentFamily };

	//On d�finit la priorit� de la queue � 1
	float queuePriority = 1.0f;

	//On cr�e les informations pour les diff�rentes familles de queue
	for (int queueFamily : uniqueQueueFamilies) {

		VkDeviceQueueCreateInfo queueCreateInfo = {};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = queueFamily;
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.pQueuePriorities = &queuePriority;

		queueCreateInfos.push_back(queueCreateInfo);

	}

	VkPhysicalDeviceFeatures deviceFeatures = {};
	deviceFeatures.samplerAnisotropy = VK_TRUE;

	//On sp�cifie les param�tres du device logique que l'on souhaite faire
	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

	createInfo.pQueueCreateInfos = queueCreateInfos.data();
	createInfo.queueCreateInfoCount = (uint32_t)queueCreateInfos.size();

	createInfo.pEnabledFeatures = &deviceFeatures;

	//On active les extensions dont on a besoin
	createInfo.enabledExtensionCount = deviceExtensions.size();
	createInfo.ppEnabledExtensionNames = deviceExtensions.data();

	//Si les validation layers sont activ�s
	if (enableValidationLayers) {
		createInfo.enabledLayerCount = validationLayers.size();
		createInfo.ppEnabledLayerNames = validationLayers.data();
	}
	else {
		createInfo.enabledExtensionCount = 0;
	}

	//On essaye de cr�er le device logique
	if (vkCreateDevice(physicalDevice, &createInfo, nullptr, &logicalDevice) != VK_SUCCESS) {
		throw std::runtime_error("failed to create a logicial device");
	}

	//Nous permet de cr�er le lien entre les queues du logique device et celles que l'on a cr�e
	vkGetDeviceQueue(logicalDevice, indices.graphicsFamily, 0, graphicsQueue);
	vkGetDeviceQueue(logicalDevice, indices.presentFamily, 0, presentQueue);

}

/**
	Permet de savoir si le device peut correspondre
*/
bool Device::isDeviceSuitable(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface) {

	QueueFamilyIndices indices = findQueueFamilies(physicalDevice, surface);

	bool extensionSupported = checkDeviceExtensionSupport(physicalDevice);

	bool swapChainAdequate = false;

	//Si la carte supporte toutes les extensions requises
	if (extensionSupported) {
		//On v�rifie que la carte supporte les formats et les modes de pr�sentations pour la swap chain
		SwapChainSupportDetails swapChainSupport = querySwapChainSupport(physicalDevice, surface);
		swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
	}

	VkPhysicalDeviceFeatures supportedFeatures;
	vkGetPhysicalDeviceFeatures(physicalDevice, &supportedFeatures);

	return indices.isComplete() && extensionSupported && swapChainAdequate && supportedFeatures.samplerAnisotropy;

}

/**
Renvoie la liste des QueueFamilies support� par la carte graphique
*/
QueueFamilyIndices Device::findQueueFamilies(VkSurfaceKHR surface) {

	QueueFamilyIndices indices;

	//On r�cup�re la liste des QueueFamilies support�s par la carte graphique
	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);

	std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilies.data());

	int i = 0;
	for (const auto &queueFamily : queueFamilies) {

		//On v�rifie si la carte supporte VK_QUEUE_GRAPHICS
		if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			indices.graphicsFamily = i;
		}

		//On v�rifie ensuite si la carte peut faire passer une image � la surface
		VkBool32 presentSupport = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, surface, &presentSupport);

		if (queueFamily.queueCount > 0 && presentSupport) {
			indices.presentFamily = i;
		}

		if (indices.isComplete()) {
			break;
		}

		i++;

	}

	return indices;

}
/**
Renvoie la liste des QueueFamilies support� par la carte graphique
*/
QueueFamilyIndices Device::findQueueFamilies(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface) {

	QueueFamilyIndices indices;

	//On r�cup�re la liste des QueueFamilies support�s par la carte graphique
	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);

	std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilies.data());

	int i = 0;
	for (const auto &queueFamily : queueFamilies) {

		//On v�rifie si la carte supporte VK_QUEUE_GRAPHICS
		if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			indices.graphicsFamily = i;
		}

		//On v�rifie ensuite si la carte peut faire passer une image � la surface
		VkBool32 presentSupport = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, surface, &presentSupport);

		if (queueFamily.queueCount > 0 && presentSupport) {
			indices.presentFamily = i;
		}

		if (indices.isComplete()) {
			break;
		}

		i++;

	}

	return indices;

}

/**
Permet de savoir si la carte pass�e en param�tre supporte les extensions
*/
bool Device::checkDeviceExtensionSupport(VkPhysicalDevice physicalDevice) {

	//On r�cup�re les extensions support�s par la carte
	uint32_t extensionCount;
	vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &extensionCount, nullptr);

	std::vector<VkExtensionProperties> availableExtensions(extensionCount);
	vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &extensionCount, availableExtensions.data());

	//On cr�e une copie des extensions requises
	std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());

	//Pour chaque extension support� on essaye de supprimer 
	for (const auto &extension : availableExtensions) {
		requiredExtensions.erase(extension.extensionName);
	}

	return requiredExtensions.empty();

}

/**
Permet de r�cup�rer la liste des propri�t�s de swap chain support� par la carte
*/
SwapChainSupportDetails Device::querySwapChainSupport(VkSurfaceKHR surface) {

	SwapChainSupportDetails details;

	//On r�cup�re la liste des capacit�s
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &details.capabilities);

	//TTout d'abord on s'occupe des propri�t�s de format
	uint32_t formatCount;
	vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, nullptr);

	if (formatCount != 0) {
		details.formats.resize(formatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, details.formats.data());
	}

	//Puis des propri�t�s des modes de pr�sentation
	uint32_t presentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, nullptr);

	if (presentModeCount != 0) {
		details.presentModes.resize(presentModeCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, details.presentModes.data());
	}

	return details;

}
/**
Permet de r�cup�rer la liste des propri�t�s de swap chain support� par la carte
*/
SwapChainSupportDetails Device::querySwapChainSupport(VkPhysicalDevice device, VkSurfaceKHR surface) {

	SwapChainSupportDetails details;

	//On r�cup�re la liste des capacit�s
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);

	//TTout d'abord on s'occupe des propri�t�s de format
	uint32_t formatCount;
	vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);

	if (formatCount != 0) {
		details.formats.resize(formatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, details.formats.data());
	}

	//Puis des propri�t�s des modes de pr�sentation
	uint32_t presentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);

	if (presentModeCount != 0) {
		details.presentModes.resize(presentModeCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, details.presentModes.data());
	}

	return details;

}

uint32_t Device::findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) {

	//On r�cup�re les types de m�moires disponibles
	VkPhysicalDeviceMemoryProperties memProperties;
	vkGetPhysicalDeviceMemoryProperties(getPhysicalDevice(), &memProperties);

	//On parcrout chaque type
	for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {

		if (typeFilter & (1 << i) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
			return i;
		}

	}

	throw std::runtime_error("failed to find suitable memory type");

}

void Device::cleanLogicalDevice() {

	vkDestroyDevice(logicalDevice, nullptr);

}

VkPhysicalDevice Device::getPhysicalDevice() {
	return this->physicalDevice;
}

VkDevice Device::getLogicalDevice() {
	return this->logicalDevice;
}