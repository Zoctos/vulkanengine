#include "TextureImage.h"



TextureImage::TextureImage(Device *device, uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties)
{
	
	//On configure les param�tres de notre image
	VkImageCreateInfo imageInfo = {};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = VK_IMAGE_TYPE_2D;
	imageInfo.extent.width = width;
	imageInfo.extent.height = height;
	imageInfo.extent.depth = 1;
	imageInfo.mipLevels = 1;
	imageInfo.arrayLayers = 1;
	imageInfo.format = format;
	imageInfo.tiling = tiling;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	imageInfo.usage = usage;
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;

	//On cr�e l'image
	if (vkCreateImage(device->getLogicalDevice(), &imageInfo, nullptr, &textureImage) != VK_SUCCESS) {
		throw std::runtime_error("failed to create image !");
	}

	//On configure les param�tres pour l'allocation de la m�moire pour l'image
	VkMemoryRequirements memRequirements;
	vkGetImageMemoryRequirements(device->getLogicalDevice(), textureImage, &memRequirements);

	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = device->findMemoryType(memRequirements.memoryTypeBits, properties);

	//On essaye d'allouer la m�moire pour l'image
	if (vkAllocateMemory(device->getLogicalDevice(), &allocInfo, nullptr, &textureImageMemory) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate image memory !");
	}

	vkBindImageMemory(device->getLogicalDevice(), textureImage, textureImageMemory, 0);

}


TextureImage::~TextureImage()
{
}

/**
	Permet de cr�er l'ImageView qui permet de r�cup�rer la texture
*/
void TextureImage::createTextureImageView(Device *device, VkFormat format, VkImageAspectFlags aspectFlags) {

	VkImageViewCreateInfo viewInfo = {};
	viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewInfo.image = textureImage;
	viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	viewInfo.format = format;
	viewInfo.subresourceRange.aspectMask = aspectFlags;
	viewInfo.subresourceRange.baseMipLevel = 0;
	viewInfo.subresourceRange.levelCount = 1;
	viewInfo.subresourceRange.baseArrayLayer = 0;
	viewInfo.subresourceRange.layerCount = 1;

	if (vkCreateImageView(device->getLogicalDevice(), &viewInfo, nullptr, &textureView) != VK_SUCCESS) {
		throw std::runtime_error("failed to create texture image view !");
	}

}

/**
	Permet de cr�er le sampler qui permettra de lire les couleurs � partir des shaders
*/
void TextureImage::createTextureSampler(Device *device) {

	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.magFilter = VK_FILTER_LINEAR;
	samplerInfo.minFilter = VK_FILTER_LINEAR;

	//On r�p�te la texture sur les trois axes si elle est trop petite
	samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;

	//Configuration du filtre anisotropique
	samplerInfo.anisotropyEnable = VK_TRUE;
	samplerInfo.maxAnisotropy = 16;

	samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
	
	//Les coordonn�es de textures sont entre 0 et 1
	samplerInfo.unnormalizedCoordinates = VK_FALSE;

	samplerInfo.compareEnable = VK_FALSE;
	samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;

	samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	samplerInfo.mipLodBias = 0.0f;
	samplerInfo.minLod = 0.0f;
	samplerInfo.maxLod = 0.0f;

	//On essaye de cr�er le sampler
	if (vkCreateSampler(device->getLogicalDevice(), &samplerInfo, nullptr, &textureSampler) != VK_SUCCESS) {
		throw std::runtime_error("failed to create texture sampler !");
	}
	
	haveSampler = true;

}

void TextureImage::cleanTextureImage(VkDevice device) {

	if(haveSampler)
		vkDestroySampler(device, textureSampler, nullptr);

	vkDestroyImageView(device, textureView, nullptr);

	vkDestroyImage(device, textureImage, nullptr);
	vkFreeMemory(device, textureImageMemory, nullptr);

}

VkImage TextureImage::getImage() {
	return this->textureImage;
}

VkImageView TextureImage::getTextureView() {
	return this->textureView;
}

VkSampler TextureImage::getTextureSampler() {
	return this->textureSampler;
}

/**
Renvoie le meilleur format disponible pour le depth buffering
*/
VkFormat TextureImage::findDepthFormat(Device *device) {

	return findSupportedFormat(device, { VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT },
		VK_IMAGE_TILING_OPTIMAL,
		VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);

}

/**
Renvoie le meilleur format parmis ceux pass�s en param�tres
*/
VkFormat TextureImage::findSupportedFormat(Device *device, const std::vector<VkFormat> &candidates, VkImageTiling tiling, VkFormatFeatureFlags features) {

	for (VkFormat format : candidates) {

		VkFormatProperties props;
		vkGetPhysicalDeviceFormatProperties(device->getPhysicalDevice(), format, &props);

		if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
			return format;
		}
		else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
			return format;
		}

	}

	throw std::runtime_error("failed to find supported format !");

}