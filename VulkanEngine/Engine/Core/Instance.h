#pragma once

#include <vulkan\vulkan.h>

#include <iostream>
#include <vector>

class Instance
{
public:
	Instance();
	~Instance();

	void initInstance(VkApplicationInfo &appInfo, std::vector<const char*> extensions, bool enableValidationLayers, std::vector<const char*> validationLayers);

	VkInstance getInstanceVk();

private:
	VkInstance instance;
};

