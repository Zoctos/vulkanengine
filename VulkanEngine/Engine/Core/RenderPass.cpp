#include "RenderPass.h"



RenderPass::RenderPass()
{
}


RenderPass::~RenderPass()
{
}


void RenderPass::createRenderPass(Device *device, VkFormat swapChainImageFormat) {

	//D�finition du color buffer attachment
	VkAttachmentDescription colorAttachment = {};
	colorAttachment.format = swapChainImageFormat;
	colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT; //1 seul sample car pas de multisampling
	colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR; //a chaque nouvelle frame on initiale le framebuffer avec une couleur noire
	colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR; //indique que les images sont ensuite mis dans une swap chain

	//D�finition de la color attachment pour le subpass
	VkAttachmentReference colorAttachmentRef = {};
	colorAttachmentRef.attachment = 0;
	colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	//D�finition du depth buffer attachment
	VkAttachmentDescription depthAttachment = {};
	depthAttachment.format = TextureImage::findDepthFormat(device);
	depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
	depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	VkAttachmentDescription postAttachment = {};
	postAttachment.format = swapChainImageFormat;
	postAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
	postAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
	postAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	postAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	postAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	postAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	postAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	//D�finition de la r�f�rence d'attachment pour le depth buffer
	VkAttachmentReference depthAttachmentRef = {};
	depthAttachmentRef.attachment = 1;
	depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	//Description du subpass
	VkSubpassDescription subpass = {};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &colorAttachmentRef;
	subpass.pDepthStencilAttachment = &depthAttachmentRef;

	//Description de l'attachment du postprocess
	VkAttachmentReference postAttachmentRef = {};
	postAttachmentRef.attachment = 1;
	postAttachmentRef.layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

	//Description du subpass pour le postprocess
	VkSubpassDescription subpasspost = {};
	subpasspost.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpasspost.inputAttachmentCount = 1;
	subpasspost.pInputAttachments = &postAttachmentRef;
	subpasspost.colorAttachmentCount = 3;
	subpasspost.pColorAttachments = &colorAttachmentRef;

	//Liste des subpass
	std::vector<VkSubpassDescription> allSubpass;
	allSubpass.push_back(subpass);
	//allSubpass.push_back(subpasspost);

	//On pr�pare les informations de d�pendance des subpass
	dependencies = {};

	addSubpassDependency(VK_SUBPASS_EXTERNAL, 0, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, VK_ACCESS_MEMORY_READ_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, (VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT));
	//addSubpassDependency(0, 1, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, VK_ACCESS_SHADER_READ_BIT);
	//addSubpassDependency(1, VK_SUBPASS_EXTERNAL, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, VK_ACCESS_MEMORY_READ_BIT);

	//Initialisation du render pass et association � la d�pendance
	std::array<VkAttachmentDescription, 2> attachments = { colorAttachment, depthAttachment };
	VkRenderPassCreateInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
	renderPassInfo.pAttachments = attachments.data();
	renderPassInfo.subpassCount = static_cast<uint32_t>(allSubpass.size());
	renderPassInfo.pSubpasses = allSubpass.data();
	renderPassInfo.dependencyCount = static_cast<uint32_t>(dependencies.size());;
	renderPassInfo.pDependencies = dependencies.data();

	//Cr�ation du render pass
	if (vkCreateRenderPass(device->getLogicalDevice(), &renderPassInfo, nullptr, &renderPass) != VK_SUCCESS) {
		throw std::runtime_error("failed to create render pass !");
	}

}

void RenderPass::addSubpassDependency(uint32_t srcSubpass, uint32_t dstSubpass, VkPipelineStageFlagBits srcStageMask, VkAccessFlags srcAccessMask, VkPipelineStageFlagBits dstStageMask,
	VkAccessFlags dstAccessMask) {

	VkSubpassDependency dependency = {};
	dependency.srcSubpass = srcSubpass;
	dependency.dstSubpass = dstSubpass;
	dependency.srcStageMask = srcStageMask;
	dependency.srcAccessMask = srcAccessMask;
	dependency.dstStageMask = dstStageMask;
	dependency.dstAccessMask = dstAccessMask;
	dependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	dependencies.push_back(dependency);

}

void RenderPass::cleanRenderPass(VkDevice logicalDevice) {

	vkDestroyRenderPass(logicalDevice, renderPass, nullptr);

}

VkRenderPass RenderPass::getRenderPass() {
	return this->renderPass;
}