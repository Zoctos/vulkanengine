#pragma once

#include <vulkan\vulkan.h>

#include <iostream>

#include "Device.h"

class TextureImage
{
public:
	TextureImage(Device *device, uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties);
	~TextureImage();

	void createTextureImageView(Device *device, VkFormat format, VkImageAspectFlags aspectFlags);
	void createTextureSampler(Device *device);

	void cleanTextureImage(VkDevice device);

	static VkFormat findDepthFormat(Device *device);
	static VkFormat TextureImage::findSupportedFormat(Device *device, const std::vector<VkFormat> &candidates, VkImageTiling tiling, VkFormatFeatureFlags features);

	VkImage getImage();
	VkImageView getTextureView();
	VkSampler getTextureSampler();

private:
	VkImage textureImage;
	VkImageView textureView;
	VkSampler textureSampler;
	VkDeviceMemory textureImageMemory;

	bool haveSampler = false;
};

