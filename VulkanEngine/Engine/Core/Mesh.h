#pragma once

#include <stb_image.h>

#include <string>
#include <unordered_map>

#include "Material.h"

class Mesh
{
public:
	Mesh(std::string modelName, Material *material);
	~Mesh();

	void loadMesh();
	void Mesh::initPipelineGraphic(Device *device, SwapChain *swapChain, RenderPass* renderPass, VkCommandPool commandPool, VkQueue graphicsQueue);
	
	void translate(glm::vec3 t);

	std::vector<Vertice> getVertices();
	std::vector<uint32_t> getIndices();
	Material *getMaterial();

private:
	//------------------
	std::string modelName;
	
	std::vector<Vertice> vertices;
	std::vector<uint32_t> indices;

	const int WIDTH = 800;
	const int HEIGHT = 600;

	Material *material;

};

