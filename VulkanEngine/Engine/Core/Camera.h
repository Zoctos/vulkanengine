#pragma once

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

class Camera
{
public:
	Camera(glm::vec3 pos, glm::vec3 lookAt, glm::vec3 cameraFront, glm::vec3 cameraUp);
	~Camera();

	void defineCameraOrientation(double xpos, double ypos);

	void update();

	glm::vec3 pos;
	glm::vec3 lookAt;
	glm::vec3 cameraFront;
	glm::vec3 cameraUp;

	bool isFirstOrientation;
	float lastX;
	float lastY;
	float pitch;
	float yaw;

private:
	


};

