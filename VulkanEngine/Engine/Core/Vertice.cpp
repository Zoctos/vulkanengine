#include "Vertice.h"

Vertice::Vertice() {

	this->pos = glm::vec3(0.0);
	this->color = glm::vec3(0.0);
	this->texCoord = glm::vec2(0.0);
	this->normal = glm::vec3(0.0f);

}

Vertice::Vertice(glm::vec3 pos, glm::vec3 color, glm::vec2 texCoord, glm::vec3 normal)
{
	this->pos = pos;
	this->color = color;
	this->texCoord = texCoord;
	this->normal = normal;
}

void Vertice::setPos(glm::vec3 pos) {
	this->pos = pos;
}

void Vertice::setColor(glm::vec3 color) {
	this->color = color;
}

void Vertice::setTexCoord(glm::vec2 texCoord) {
	this->texCoord = texCoord;
}

void Vertice::setNormal(glm::vec3 normal) {
	this->normal = normal;
}