#pragma once

#include <vulkan\vulkan.h>

#include <array>
#include <vector>

#include "Device.h"
#include "SwapChain.h"
#include "TextureImage.h"

class Framebuffer
{
public:
	Framebuffer();
	~Framebuffer();

	void createFramebuffers(Device *device, SwapChain *swapChain, VkRenderPass renderPass, TextureImage *textureDepth);
	
	void cleanup(Device *device);

	std::vector<VkFramebuffer> getFramebuffers();

private:
	std::vector<VkFramebuffer> swapChainFramebuffers;
};

