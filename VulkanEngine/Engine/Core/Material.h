#pragma once

#include <vulkan\vulkan.h>

#include "GraphicPipeline.h"

class Material
{
public:
	Material(std::string materialName);
	~Material();

	void Material::initPipelineGraphic(Device *device, SwapChain *swapChain, RenderPass* renderPass, VkCommandPool commandPool, VkQueue graphicsQueue, std::vector<Vertice> vertices, std::vector<uint32_t> indices);
	
	std::string textureName;

	GraphicPipeline *getGraphicPipeline();

private:
	std::string materialName;

	GraphicPipeline *graphicPipeline;
};

