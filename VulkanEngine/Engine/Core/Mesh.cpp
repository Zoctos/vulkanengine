#include "Mesh.h"

#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>

Mesh::Mesh(std::string modelName, Material *material)
{

	this->modelName = modelName;
	this->material = material;

	loadMesh();

}


Mesh::~Mesh()
{
	delete material;

}

/**
	Permet de charger le fichier obj du mesh et de r�cup�rer ses vertices et ses indices
*/
void Mesh::loadMesh() {

	std::string path = "Models/" + this->modelName;

	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string err;

	if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &err, path.c_str())) {
		throw std::runtime_error(err);
	}

	std::unordered_map<Vertice, uint32_t> uniqueVertices = {};

	bool haveNormal = false;

	if (attrib.normals.size() > 0) {
		haveNormal = true;
		std::cout << "normales trouvees" << std::endl;
	}

	for (const auto &shape : shapes) {
		for (const auto &index : shape.mesh.indices) {

			Vertice v;
			v.setPos(glm::vec3(attrib.vertices[3 * index.vertex_index + 0], attrib.vertices[3 * index.vertex_index + 1], attrib.vertices[3 * index.vertex_index + 2]));
			v.setTexCoord(glm::vec2(attrib.texcoords[2 * index.texcoord_index + 0], 1.0f - attrib.texcoords[2 * index.texcoord_index + 1]));
			//v.setColor(glm::vec3(1.0f));
			v.setColor(glm::vec3(v.texCoord, 1.0f));

			if(haveNormal){
				v.setNormal(glm::vec3(attrib.normals[3 * index.vertex_index + 0], attrib.normals[3 * index.vertex_index + 1], attrib.normals[3 * index.vertex_index + 2]));
			}
			

			if (uniqueVertices.count(v) == 0) {
				uniqueVertices[v] = static_cast<uint32_t>(vertices.size());
				vertices.push_back(v);
			}

			indices.push_back(uniqueVertices[v]);

		}
	}

}

void Mesh::initPipelineGraphic(Device *device, SwapChain *swapChain, RenderPass* renderPass, VkCommandPool commandPool, VkQueue graphicsQueue) {

	material->initPipelineGraphic(device, swapChain, renderPass, commandPool, graphicsQueue, vertices, indices);

}

void Mesh::translate(glm::vec3 t) {

	for (int i = 0; i < vertices.size(); ++i) {
		vertices[i].pos = vertices[i].pos + t;
	}

}

std::vector<Vertice> Mesh::getVertices() {
	return this->vertices;
}

std::vector<uint32_t> Mesh::getIndices() {
	return this->indices;
}

Material* Mesh::getMaterial() {
	return this->material;
}