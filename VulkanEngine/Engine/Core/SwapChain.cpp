#include "SwapChain.h"



SwapChain::SwapChain()
{
}


SwapChain::~SwapChain()
{
}

/**
	Permet de cr�er la swap chain
*/
void SwapChain::createSwapChain(SwapChainSupportDetails swapChainSupport, QueueFamilyIndices indices, VkSurfaceKHR surface, GLFWwindow *window, VkDevice logicalDevice) {

	//R�cup�ration des propri�t�s de la swap chain

	VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
	VkPresentModeKHR presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
	VkExtent2D extent = chooseSwapExtent(swapChainSupport.capabilities, window);

	//On d�finit le nombre minimum d'images pour que la swap chain fonctionne correctement
	uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;
	if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount) {
		imageCount = swapChainSupport.capabilities.maxImageCount;
	}

	//Cr�ation de la structure contenant les informations de la swapchain
	VkSwapchainCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = surface;

	createInfo.minImageCount = imageCount;
	createInfo.imageFormat = surfaceFormat.format;
	createInfo.imageColorSpace = surfaceFormat.colorSpace;
	createInfo.imageExtent = extent;
	createInfo.imageArrayLayers = 1; //toujours 1 sauf dans le cas d'une application en 3D stereoscopic

	//on indique que l'image doit �tre directement rendu (pas de post-processing possible ; doit utiliser : VK_IMAGE_USAGE_TRANSFER_DST_BIT
	//createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	createInfo.imageUsage = VK_IMAGE_USAGE_TRANSFER_DST_BIT;

	//On r�cup�re les familles de queue
	uint32_t queueFamilyIndices[] = { (uint32_t)indices.graphicsFamily, (uint32_t)indices.presentFamily };

	//Si les familles de queue sont diff�rentes
	if (indices.graphicsFamily != indices.presentFamily) {
		createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		createInfo.queueFamilyIndexCount = 2;
		createInfo.pQueueFamilyIndices = queueFamilyIndices;
	}
	else {
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		createInfo.queueFamilyIndexCount = 0;
		createInfo.pQueueFamilyIndices = nullptr;
	}

	//Sp�cifie les transformations � appliquer sur les images de la cha�ne (une rotation par exemple)
	createInfo.preTransform = swapChainSupport.capabilities.currentTransform;

	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR; //on ignore le canal alpha

	createInfo.presentMode = presentMode;
	createInfo.clipped = VK_TRUE;

	if (vkCreateSwapchainKHR(logicalDevice, &createInfo, nullptr, &swapChain) != VK_SUCCESS) {
		throw std::runtime_error("failed to create swap chain !");
	}

	//On r�cup�re les images de la swapchain, on redimensionne le conteneur et on rer�cup�re les liens
	vkGetSwapchainImagesKHR(logicalDevice, swapChain, &imageCount, nullptr);
	swapChainImages.resize(imageCount);
	vkGetSwapchainImagesKHR(logicalDevice, swapChain, &imageCount, swapChainImages.data());

	swapChainImageFormat = surfaceFormat.format;
	swapChainExtent = extent;

}

void SwapChain::createSwapChainImageView(Device *device) {

	//On redimensionne la liste des image view
	swapChainImageViews.resize(getSwapChainImages().size());

	//Pour chaque image de la swap chain
	for (uint32_t i = 0; i < getSwapChainImages().size(); i++) {

		//On cr�e la structure contenant les infos de l'image view
		VkImageViewCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		createInfo.image = getSwapChainImages()[i];

		//On sp�cifie le type d'image et le format
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D; //on specifie qu'on utilise des textures 2D
		createInfo.format = getSwapChainImageFormat();

		//On indique que les canaux sont uniques
		createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

		//On d�finit les parties d'images auquel on pourra acc�der
		createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		createInfo.subresourceRange.baseMipLevel = 0;
		createInfo.subresourceRange.levelCount = 1;
		createInfo.subresourceRange.baseArrayLayer = 0;
		createInfo.subresourceRange.layerCount = 1;

		//On essaye de cr�er l'image view
		if (vkCreateImageView(device->getLogicalDevice(), &createInfo, nullptr, &swapChainImageViews[i]) != VK_SUCCESS) {
			throw std::runtime_error("failed to create image views !");
		}

	}

}

/**
Permet de d�terminer le format de la surface pour la swap chain
*/
VkSurfaceFormatKHR SwapChain::chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR> &availableFormats) {

	//Si on a d�j� un format dans la liste et qu'il est indf�ni
	if (availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED) {
		//On indique qu'on sera avec du RGBA avec 8 bits par canaux et avec du SRGB
		return { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
	}

	//Si on a rien on essaye de trouver ce format
	for (const auto &availableFormat : availableFormats) {
		if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
			return availableFormat;
		}
	}

	//Sinon on renvoie le format qui est disponible
	return availableFormats[0];

}

/**
Permet de d�terminer le mode de pr�sentation des images pour la swap chain
*/
VkPresentModeKHR SwapChain::chooseSwapPresentMode(const std::vector<VkPresentModeKHR> availablePresentModes) {

	//On pr�pare le mode de pr�sentation qui est compatible avec toutes les configs
	VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;

	for (auto const &availablePresentMode : availablePresentModes) {

		//Si le mode MAILBOX est disponible on le choisit directement
		if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
			return availablePresentMode;
		}
		else if (availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR) {
			bestMode = availablePresentMode;
		}
	}

	return bestMode;
}

/**
Permet de d�finir la r�solution des images de la swap chain
*/
VkExtent2D SwapChain::chooseSwapExtent(const VkSurfaceCapabilitiesKHR &capabilities, GLFWwindow *window) {

	//Si la r�solution est diff�rente du maximum possible
	if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
		return capabilities.currentExtent;
	}
	else {
		//on r�cup�re la taille de la fen�tre
		int width, height;
		glfwGetWindowSize(window, &width, &height);

		//On d�finit que la taille doit correspondre � celle de la fen�tre
		VkExtent2D actualExtent = { width, height };

		actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
		actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

		return actualExtent;
	}

}

void SwapChain::cleanSwapChain(VkDevice logicalDevice) {

	vkDestroySwapchainKHR(logicalDevice, swapChain, nullptr);

}

VkSwapchainKHR SwapChain::getSwapChain() {
	return this->swapChain;
}

std::vector<VkImage> SwapChain::getSwapChainImages() {
	return this->swapChainImages;
}

std::vector<VkImageView> SwapChain::getSwapChainImageViews() {
	return this->swapChainImageViews;
}

VkFormat SwapChain::getSwapChainImageFormat() {
	return this->swapChainImageFormat;
}

VkExtent2D SwapChain::getSwapChainExtent() {
	return this->swapChainExtent;
}