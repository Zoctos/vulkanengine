#pragma once

#include <vulkan\vulkan.h>

#include <vector>

class Shader
{
public:
	Shader(std::vector<char> code);
	~Shader();

	void createShader(VkDevice logicalDevice);

	VkShaderModule getShaderModule();

private :
	std::vector<char> code;
	VkShaderModule module;


};

