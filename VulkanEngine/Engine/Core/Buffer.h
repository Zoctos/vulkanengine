#pragma once

#include <vulkan\vulkan.h>

#include "Device.h"

class Buffer
{
public:
	
	static VkCommandBuffer beginSimpleTimeCommands(Device *device, VkCommandPool commandPool);
	static void endSingleTimeCommands(Device *device, VkCommandBuffer commandBuffer, VkCommandPool commandPool, VkQueue graphicsQueue);

	static void transitionImageLayout(Device *device, VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout, VkCommandPool commandPool, VkQueue graphicsQueue);
	static bool hasStencilComponent(VkFormat format);

	static void createBuffer(Device *device, VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer &buffer, VkDeviceMemory &bufferMemory);

	static void copyBuffer(Device *device, VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size, VkQueue graphicsQueue, VkCommandPool commandPool);
	static void copyBufferToImage(Device *device, VkBuffer buffer, VkImage image, uint32_t width, uint32_t height, VkCommandPool commandPool, VkQueue graphicsQueue);

};

