﻿#define STB_IMAGE_IMPLEMENTATION
#include "GraphicPipeline.h"

#include <stb_image.h>

#include <iostream>

GraphicPipeline::GraphicPipeline(Device *device)
{

	this->device = device;

	this->vertexShader = nullptr;
	this->geometryShader = nullptr;
	this->fragmentShader = nullptr;

	this->tImage = nullptr;

	this->subpass = 0;
}


GraphicPipeline::~GraphicPipeline()
{

	if (this->vertexShader != nullptr) {
		delete this->vertexShader;
	}
	if (this->geometryShader != nullptr) {
		delete this->geometryShader;
	}
	if (this->fragmentShader != nullptr) {
		delete this->fragmentShader;
	}

	if (tImage != nullptr) {
		tImage->cleanTextureImage(device->getLogicalDevice());
	}

	vkDestroyDescriptorPool(device->getLogicalDevice(), descriptorPool, nullptr);

	vkDestroyDescriptorSetLayout(device->getLogicalDevice(), descriptorSetLayout, nullptr);
	vkDestroyBuffer(device->getLogicalDevice(), uniformBuffer, nullptr);
	vkFreeMemory(device->getLogicalDevice(), uniformBufferMemory, nullptr);

	vkDestroyBuffer(device->getLogicalDevice(), lightBuffer, nullptr);
	vkFreeMemory(device->getLogicalDevice(), lightBufferMemory, nullptr);

	vkDestroyBuffer(device->getLogicalDevice(), indexBuffer, nullptr);
	vkFreeMemory(device->getLogicalDevice(), indexBufferMemory, nullptr);

	vkDestroyBuffer(device->getLogicalDevice(), vertexBuffer, nullptr);
	vkFreeMemory(device->getLogicalDevice(), vertexBufferMemory, nullptr);

}

void GraphicPipeline::setVertexShader(std::vector<char> code) {

	if (this->vertexShader != nullptr) {
		delete this->vertexShader;
	}

	this->vertexShader = new Shader(code);

}

void GraphicPipeline::setFragmentShader(std::vector<char> code) {

	if (this->fragmentShader != nullptr) {
		delete this->fragmentShader;
	}

	this->fragmentShader = new Shader(code);

}

void GraphicPipeline::createDescriptorSetLayout() {

	layoutBinding = {};

	//Préparation des descriptor de layout (ubo, sampler et light)
	addDescriptorSetLayout(0, 1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT, nullptr);

	if (tImage != nullptr) {
		addDescriptorSetLayout(1, 1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT, nullptr);
	}

	addDescriptorSetLayout(2, 1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_FRAGMENT_BIT, nullptr);

	VkDescriptorSetLayoutCreateInfo layoutInfo = {};
	layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.bindingCount = static_cast<uint32_t>(layoutBinding.size());
	layoutInfo.pBindings = layoutBinding.data();

	//On crée le descriptor
	if (vkCreateDescriptorSetLayout(device->getLogicalDevice(), &layoutInfo, nullptr, &descriptorSetLayout) != VK_SUCCESS) {
		throw std::runtime_error("failed to create descriptor set layout !");
	}

}

void GraphicPipeline::createGraphicPipeline(VkPipelineViewportStateCreateInfo viewportState, VkRenderPass renderPass) {

	std::vector<VkPipelineShaderStageCreateInfo> shaderList;

	VkPipelineShaderStageCreateInfo vertexInfo = {};
	VkPipelineShaderStageCreateInfo geometryInfo = {};
	VkPipelineShaderStageCreateInfo fragmentInfo = {};

	if (this->vertexShader != nullptr) {

		vertexShader->createShader(device->getLogicalDevice());

		vertexInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		vertexInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
		vertexInfo.module = this->vertexShader->getShaderModule();
		vertexInfo.pName = "main";

		shaderList.push_back(vertexInfo);
	}

	if (this->geometryShader != nullptr) {

		geometryShader->createShader(device->getLogicalDevice());

		geometryInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		geometryInfo.stage = VK_SHADER_STAGE_GEOMETRY_BIT;
		geometryInfo.module = this->geometryShader->getShaderModule();
		geometryInfo.pName = "main";

		shaderList.push_back(geometryInfo);
	}

	if (this->fragmentShader != nullptr) {

		fragmentShader->createShader(device->getLogicalDevice());

		fragmentInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		fragmentInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
		fragmentInfo.module = this->fragmentShader->getShaderModule();
		fragmentInfo.pName = "main";

		shaderList.push_back(fragmentInfo);
	}

	VkPipelineShaderStageCreateInfo *list = &shaderList[0];

	//On récupère les descriptions de binding et d'attribut pour les vertex
	auto bindingDescription = Vertice::getBindingDescription();
	auto attributeDescriptions = Vertice::getAttributeDescriptions();

	//On crée la structure qui contient les informations concernant les vertex
	VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
	vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputInfo.vertexBindingDescriptionCount = 1;
	vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
	vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
	vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

	//On indique que l'on veut relier les points par triangles
	VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssembly.primitiveRestartEnable = VK_FALSE;

	//Configuration du rasterizer
	VkPipelineRasterizationStateCreateInfo rasterizer = {};
	rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizer.depthClampEnable = VK_FALSE;
	rasterizer.rasterizerDiscardEnable = VK_FALSE;
	rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizer.lineWidth = 1.0f;
	rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
	rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	rasterizer.depthBiasEnable = VK_FALSE;

	//Configuration du multisampling (qui permet notamment de faire de l'anti-aliasing en faisant ce qu'il faut)
	VkPipelineMultisampleStateCreateInfo multisampling = {};
	multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampling.sampleShadingEnable = VK_FALSE;
	multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

	//Configuration du depth stencil
	VkPipelineDepthStencilStateCreateInfo depthStencil = {};
	depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	depthStencil.depthTestEnable = VK_TRUE;
	depthStencil.depthWriteEnable = VK_TRUE;
	depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
	depthStencil.depthBoundsTestEnable = VK_FALSE;
	depthStencil.stencilTestEnable = VK_FALSE;

	//Configuration du color blending par framebuffer
	VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
	colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colorBlendAttachment.blendEnable = VK_FALSE;

	//Configuration du color blending global
	VkPipelineColorBlendStateCreateInfo colorBlending = {};
	colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlending.logicOpEnable = VK_FALSE;
	colorBlending.attachmentCount = 1;
	colorBlending.pAttachments = &colorBlendAttachment;

	//Configuration du pipeline layout et on indique les descriptors qui seront utilisés
	VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = 1;
	pipelineLayoutInfo.pSetLayouts = &descriptorSetLayout;

	//On essaye de créer le pipeline layout
	if (vkCreatePipelineLayout(device->getLogicalDevice(), &pipelineLayoutInfo, nullptr, &pipelineLayout) != VK_SUCCESS) {
		throw std::runtime_error("failed to create pipeline layout !");
	}

	//Configuration du pipeline graphique
	VkGraphicsPipelineCreateInfo pipelineInfo = {};
	pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineInfo.stageCount = 2;
	pipelineInfo.pStages = list;

	pipelineInfo.pVertexInputState = &vertexInputInfo;
	pipelineInfo.pInputAssemblyState = &inputAssembly;
	pipelineInfo.pViewportState = &viewportState;
	pipelineInfo.pRasterizationState = &rasterizer;
	pipelineInfo.pMultisampleState = &multisampling;
	pipelineInfo.pColorBlendState = &colorBlending;
	pipelineInfo.pDepthStencilState = &depthStencil;

	pipelineInfo.layout = pipelineLayout;

	pipelineInfo.renderPass = renderPass;
	pipelineInfo.subpass = this->subpass;

	//Création du pipeline graphique
	if (vkCreateGraphicsPipelines(device->getLogicalDevice(), VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline) != VK_SUCCESS) {
		throw std::runtime_error("failed to create graphics pipeline !");
	}

}

/**
	Création d'une texture
*/
void GraphicPipeline::createTextureImage(std::string textureName, VkCommandPool commandPool, VkQueue graphicsQueue) {

	std::string path = "Textures/" + textureName;

	int texWidth, texHeight, texChannels;
	stbi_uc *pixels = stbi_load(path.c_str(), &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
	VkDeviceSize imageSize = texWidth * texHeight * 4;

	if (!pixels) {
		throw std::runtime_error("failed to load texture image !");
	}

	VkBuffer stagingBuffer;
	VkDeviceMemory stagingBufferMemory;

	Buffer::createBuffer(device, imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

	void *data;
	vkMapMemory(device->getLogicalDevice(), stagingBufferMemory, 0, imageSize, 0, &data);
	memcpy(data, pixels, static_cast<size_t>(imageSize));
	vkUnmapMemory(device->getLogicalDevice(), stagingBufferMemory);

	stbi_image_free(pixels);

	tImage = new TextureImage(device, texWidth, texHeight, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT |
		VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	Buffer::transitionImageLayout(device, tImage->getImage(), VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, commandPool, graphicsQueue);
	Buffer::copyBufferToImage(device, stagingBuffer, tImage->getImage(), static_cast<uint32_t>(texWidth), static_cast<uint32_t>(texHeight), commandPool, graphicsQueue);
	Buffer::transitionImageLayout(device, tImage->getImage(), VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, commandPool, graphicsQueue);

	vkDestroyBuffer(device->getLogicalDevice(), stagingBuffer, nullptr);
	vkFreeMemory(device->getLogicalDevice(), stagingBufferMemory, nullptr);

}

void GraphicPipeline::createVertexBuffer(std::vector<Vertice> *vertices, VkQueue graphicsQueue, VkCommandPool commandPool) {

	//On calcule la taille requise pour le buffer
	VkDeviceSize bufferSize = sizeof(Vertice) * vertices->size();

	VkBuffer stagingBuffer;
	VkDeviceMemory stagingBufferMemory;

	//On crée le buffer de staging
	Buffer::createBuffer(device, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
		VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

	//On copie les données des vertices dans le buffer de staging
	void *data;
	vkMapMemory(device->getLogicalDevice(), stagingBufferMemory, 0, bufferSize, 0, &data);
	memcpy(data, vertices->data(), bufferSize);
	vkUnmapMemory(device->getLogicalDevice(), stagingBufferMemory);

	//On crée le buffer de mémoire du vertex
	Buffer::createBuffer(device, bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
		VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, vertexBuffer, vertexBufferMemory);

	//On copie le contenu du buffer de staging dans le buffer de mémoire du vertex
	Buffer::copyBuffer(device, stagingBuffer, vertexBuffer, bufferSize, graphicsQueue, commandPool);

	//On détruit le buffer de staging et on libère la mémoire qui lui était allouée
	vkDestroyBuffer(device->getLogicalDevice(), stagingBuffer, nullptr);
	vkFreeMemory(device->getLogicalDevice(), stagingBufferMemory, nullptr);

}

void GraphicPipeline::createIndexBuffer(std::vector<uint32_t> indices, VkQueue graphicsQueue, VkCommandPool commandPool) {

	//On définit la taille du buffer
	VkDeviceSize bufferSize = sizeof(indices[0]) * indices.size();

	VkBuffer stagingBuffer;
	VkDeviceMemory stagingBufferMemory;

	//On crée le buffer de staging
	Buffer::createBuffer(device, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
		VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

	//On copie les données dans le buffer de staging
	void *data;
	vkMapMemory(device->getLogicalDevice(), stagingBufferMemory, 0, bufferSize, 0, &data);
	memcpy(data, indices.data(), (size_t)bufferSize);
	vkUnmapMemory(device->getLogicalDevice(), stagingBufferMemory);

	//On crée le buffer de la mémoire des index
	Buffer::createBuffer(device, bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
		VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, indexBuffer, indexBufferMemory);

	//On copie le contenu du buffer de stagging dans le buffer de la mémoire des index
	Buffer::copyBuffer(device, stagingBuffer, indexBuffer, bufferSize, graphicsQueue, commandPool);

	//On détruit le buffer de staging et on libère la mémoire qui lui était allouée
	vkDestroyBuffer(device->getLogicalDevice(), stagingBuffer, nullptr);
	vkFreeMemory(device->getLogicalDevice(), stagingBufferMemory, nullptr);

}

void GraphicPipeline::createUniformBuffer() {

	//On définit la taille du buffer
	VkDeviceSize bufferSize = sizeof(UniformBufferObject);

	//On crée le buffer
	Buffer::createBuffer(device, bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
		VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, uniformBuffer, uniformBufferMemory);

	//On définit la taille du buffer pour la lumière
	VkDeviceSize lightSize = sizeof(LightBuffer);

	Buffer::createBuffer(device, lightSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
		VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, lightBuffer, lightBufferMemory);


}

void GraphicPipeline::createDescriptorPool() {

	//On indique les différents descriptor que l'on a (un pour l'ubo, un pour le sampler, et un autre pour la lumiere)
	std::vector<VkDescriptorPoolSize> poolSizes = {};

	VkDescriptorPoolSize pUBO = {};
	pUBO.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	pUBO.descriptorCount = 1;
	poolSizes.push_back(pUBO);

	if (tImage != nullptr) {
		VkDescriptorPoolSize pTexture = {};
		pTexture.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		pTexture.descriptorCount = 1;
		poolSizes.push_back(pTexture);
	}

	VkDescriptorPoolSize pLight = {};
	pLight.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	pLight.descriptorCount = 1;
	poolSizes.push_back(pLight);

	//On configure la création du descriptor pool
	VkDescriptorPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = 1;

	//On crée le descriptor pool
	if (vkCreateDescriptorPool(device->getLogicalDevice(), &poolInfo, nullptr, &descriptorPool) != VK_SUCCESS) {
		throw std::runtime_error("failed to create descriptor pool !");
	}
}

void GraphicPipeline::createDescriptorSet() {

	//On indique le descriptor pool, le descriptor layout pour tenter d'allouer de la mémoire pour le descriptor set
	VkDescriptorSetLayout layouts[] = { descriptorSetLayout };
	VkDescriptorSetAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = descriptorPool;
	allocInfo.descriptorSetCount = 1;
	allocInfo.pSetLayouts = layouts;
	allocInfo.pNext = NULL;

	//On essaye d'allouer de la mémoire pour le descriptor set
	if (vkAllocateDescriptorSets(device->getLogicalDevice(), &allocInfo, &descriptorSet) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate descriptor set !");
	}

	//On indique le buffer pour l'UBO
	VkDescriptorBufferInfo bufferInfo = {};
	bufferInfo.buffer = uniformBuffer;
	bufferInfo.offset = 0;
	bufferInfo.range = sizeof(UniformBufferObject);

	//On prépare le buffer pour la lumière
	VkDescriptorBufferInfo lightBufferInfo = {};
	lightBufferInfo.buffer = lightBuffer;
	lightBufferInfo.offset = 0;
	lightBufferInfo.range = sizeof(LightBuffer);

	//On configure le descriptor set pour l'écriture
	descriptorWrites = {};

	addDescriptorWrite(0, 0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, &bufferInfo);

	if (tImage != nullptr) {
		//On configure le binding de l'image et du sampler pour le descriptor set
		VkDescriptorImageInfo imageInfo = {};
		imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		imageInfo.imageView = tImage->getTextureView();
		imageInfo.sampler = tImage->getTextureSampler();
		addDescriptorWriteImg(1, 0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1, &imageInfo);
	}
	
	addDescriptorWrite(2, 0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, &lightBufferInfo);

	//On applique les mises à jours
	vkUpdateDescriptorSets(device->getLogicalDevice(), static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);

}

void GraphicPipeline::addDescriptorSetLayout(int binding, int descriptorCount, VkDescriptorType descriptorType, VkShaderStageFlagBits stageFlags, VkSampler *sampler) {

	VkDescriptorSetLayoutBinding layout = {};
	layout.binding = binding;
	layout.descriptorCount = descriptorCount;
	layout.descriptorType = descriptorType;
	layout.stageFlags = stageFlags;
	layout.pImmutableSamplers = sampler;

	layoutBinding.push_back(layout);

}

void GraphicPipeline::addDescriptorWrite(int dstBinding, int dstArrayElement, VkDescriptorType descriptorType, int descriptorCount, VkDescriptorBufferInfo *pBuffer) {

	VkWriteDescriptorSet dw = {};
	dw.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	dw.dstSet = descriptorSet;
	dw.dstBinding = dstBinding;
	dw.dstArrayElement = dstArrayElement;
	dw.descriptorType = descriptorType;
	dw.descriptorCount = descriptorCount;
	dw.pBufferInfo = pBuffer;

	descriptorWrites.push_back(dw);

}

void GraphicPipeline::addDescriptorWriteImg(int dstBinding, int dstArrayElement, VkDescriptorType descriptorType, int descriptorCount, VkDescriptorImageInfo *pImage) {

	VkWriteDescriptorSet dw = {};
	dw.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	dw.dstSet = descriptorSet;
	dw.dstBinding = dstBinding;
	dw.dstArrayElement = dstArrayElement;
	dw.descriptorType = descriptorType;
	dw.descriptorCount = descriptorCount;
	dw.pImageInfo = pImage;

	descriptorWrites.push_back(dw);

}

VkDescriptorSet GraphicPipeline::getDescriptorSet() {
	return this->descriptorSet;
}

VkBuffer GraphicPipeline::getVertexBuffer() {
	return this->vertexBuffer;
}

VkBuffer GraphicPipeline::getIndexBuffer() {
	return this->indexBuffer;
}

VkDeviceMemory GraphicPipeline::getUniformBufferMemory() {
	return this->uniformBufferMemory;
}

VkPipeline GraphicPipeline::getGraphicPipeline() {
	return this->graphicsPipeline;
}

VkPipelineLayout GraphicPipeline::getPipelineLayout() {
	return this->pipelineLayout;
}

TextureImage* GraphicPipeline::getTextureImage() {
	return this->tImage;
}

VkDeviceMemory GraphicPipeline::getLightBufferMemory() {
	return this->lightBufferMemory;
}