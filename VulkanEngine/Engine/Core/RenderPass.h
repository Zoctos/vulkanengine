#pragma once

#include <iostream>
#include <vulkan\vulkan.h>

#include "Device.h"
#include "GraphicPipeline.h"

class RenderPass
{
public:
	RenderPass();
	~RenderPass();

	void createRenderPass(Device *device, VkFormat swapChainImageFormat);

	void addSubpassDependency(uint32_t srcSubpass, uint32_t dstSubpass, VkPipelineStageFlagBits srcStageMask, VkAccessFlags srcAccessMask, VkPipelineStageFlagBits dstStageMask,
		VkAccessFlags dstAccessMask);

	void cleanRenderPass(VkDevice logicalDevice);

	VkRenderPass getRenderPass();

private:
	VkRenderPass renderPass;

	std::vector<VkSubpassDependency> dependencies;
};

