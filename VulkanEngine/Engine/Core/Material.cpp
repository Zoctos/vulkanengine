#include "Material.h"

Material::Material(std::string materialName)
{
	this->materialName = materialName;
	this->textureName = "";
}


Material::~Material()
{
	delete graphicPipeline;
}

void Material::initPipelineGraphic(Device *device, SwapChain *swapChain, RenderPass* renderPass, VkCommandPool commandPool, VkQueue graphicsQueue, std::vector<Vertice> vertices, std::vector<uint32_t> indices) {

	graphicPipeline = new GraphicPipeline(device);

	if (this->textureName != "") {
		graphicPipeline->createTextureImage(this->textureName, commandPool, graphicsQueue);
		graphicPipeline->getTextureImage()->createTextureImageView(device, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT);
		graphicPipeline->getTextureImage()->createTextureSampler(device);
	}

	graphicPipeline->createDescriptorSetLayout();

	graphicPipeline->setVertexShader(Tools::readFile("Materials/" + materialName + "/Shader/vertex.glsl"));
	graphicPipeline->setFragmentShader(Tools::readFile("Materials/" + materialName + "/Shader/fragment.glsl"));

	//On cr�e le viewport
	VkViewport viewport = {};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = (float)swapChain->getSwapChainExtent().width;
	viewport.height = (float)swapChain->getSwapChainExtent().height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	//On cr�e la vue qui affiche la sc�ne (d�coupe ce qui est en dehors du cadre)
	VkRect2D scissor = {};
	scissor.offset = { 0, 0 };
	scissor.extent = swapChain->getSwapChainExtent();

	//On sp�cifie qu'on utilise le viewport qu'on a cr�e auparavant et on l'associe au scissor
	VkPipelineViewportStateCreateInfo viewportState = {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.pViewports = &viewport;
	viewportState.scissorCount = 1;
	viewportState.pScissors = &scissor;

	graphicPipeline->createGraphicPipeline(viewportState, renderPass->getRenderPass());

	graphicPipeline->createVertexBuffer(&vertices, graphicsQueue, commandPool);
	graphicPipeline->createIndexBuffer(indices, graphicsQueue, commandPool);
	graphicPipeline->createUniformBuffer();

	graphicPipeline->createDescriptorPool();
	graphicPipeline->createDescriptorSet();

}

GraphicPipeline* Material::getGraphicPipeline() {
	return this->graphicPipeline;
}