#pragma once

#include <vulkan\vulkan.h>
#include <glm\gtx\hash.hpp>

#include <vector>

#include "Light.h"

struct UniformBufferObject {
	glm::mat4 model;
	glm::mat4 view;
	glm::mat4 proj;
	glm::vec4 camera;
};

class Vertice
{
public:

	Vertice();

	Vertice(glm::vec3 pos, glm::vec3 color, glm::vec2 texCoord, glm::vec3 normal);

	glm::vec3 pos;
	glm::vec3 color;
	glm::vec2 texCoord;
	glm::vec3 normal;

	void setPos(glm::vec3 pos);
	void setColor(glm::vec3 color);
	void setTexCoord(glm::vec2 texCoord);
	void setNormal(glm::vec3 normal);

	/**
	Permet de sp�cifier les informations n�cessaires pour le binding
	*/
	static VkVertexInputBindingDescription getBindingDescription() {
		VkVertexInputBindingDescription bindingDescription = {};
		bindingDescription.binding = 0;
		bindingDescription.stride = sizeof(Vertice);
		bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		return bindingDescription;
	};

	/**
	Permet de d�crire comment sera fait le lien
	*/
	static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions() {
		std::vector<VkVertexInputAttributeDescription> attributeDescriptions(4);

		VkVertexInputAttributeDescription posAtt = {};
		posAtt.binding = 0;
		posAtt.location = 0;
		posAtt.format = VK_FORMAT_R32G32B32_SFLOAT;
		posAtt.offset = offsetof(Vertice, pos);
		attributeDescriptions[0] = posAtt;

		VkVertexInputAttributeDescription colorAtt = {};
		colorAtt.binding = 0;
		colorAtt.location = 1;
		colorAtt.format = VK_FORMAT_R32G32B32_SFLOAT;
		colorAtt.offset = offsetof(Vertice, color);
		attributeDescriptions[1] = colorAtt;

		VkVertexInputAttributeDescription texCoordAtt = {};
		texCoordAtt.binding = 0;
		texCoordAtt.location = 2;
		texCoordAtt.format = VK_FORMAT_R32G32_SFLOAT;
		texCoordAtt.offset = offsetof(Vertice, texCoord);
		attributeDescriptions[2] = texCoordAtt;

		VkVertexInputAttributeDescription normalAtt = {};
		normalAtt.binding = 0;
		normalAtt.location = 3;
		normalAtt.format = VK_FORMAT_R32G32B32_SFLOAT;
		normalAtt.offset = offsetof(Vertice, normal);
		attributeDescriptions[3] = normalAtt;

		return attributeDescriptions;
	};

	bool operator==(const Vertice &other) const {
		return pos == other.pos && color == other.color && texCoord == other.texCoord;
	};

};

namespace std {
	template<> struct hash<Vertice> {
		size_t operator()(Vertice const &v) const {
			return ((hash<glm::vec3>()(v.pos) ^
				(hash<glm::vec3>()(v.color) << 1)) >> 1) ^
				(hash<glm::vec2>()(v.texCoord) << 1);
		}
	};
}