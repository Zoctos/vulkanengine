#include "Shader.h"



Shader::Shader(std::vector<char> code)
{
	this->code = code;
	module = nullptr;
}


Shader::~Shader()
{
}

void Shader::createShader(VkDevice logicalDevice) {

	//Cr�ation de la structure contenant les informations
	VkShaderModuleCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.codeSize = code.size();

	std::vector<uint32_t> codeAligned(code.size() / sizeof(uint32_t) + 1);
	memcpy(codeAligned.data(), code.data(), code.size());
	createInfo.pCode = codeAligned.data();

	//On essaye de cr�er le shader module
	if (vkCreateShaderModule(logicalDevice, &createInfo, nullptr, &module) != VK_SUCCESS) {
		throw std::runtime_error("failed to create shader module !");
	}

}

VkShaderModule Shader::getShaderModule() {
	return this->module;
}