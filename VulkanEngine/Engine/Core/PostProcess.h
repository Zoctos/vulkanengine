#pragma once

#include "Camera.h"
#include "GraphicPipeline.h"

class PostProcess {

public:

	PostProcess(std::string name, Device *device, SwapChain *swapChain, RenderPass *renderPass, VkQueue graphicsQueue, VkCommandPool commandPool, Camera *camera);

	void render(Device *device, VkCommandBuffer commandBuffer, VkImageView view, VkCommandPool commandPool, VkQueue graphicsQueue);

	GraphicPipeline* getGraphicPipeline();

	std::vector<uint32_t> getIndices();

private:
	std::string name;
	GraphicPipeline *graphicPipeline;
	VkSampler sampler;
	
	std::vector<Vertice> vertices;
	std::vector<uint32_t> indices;

};