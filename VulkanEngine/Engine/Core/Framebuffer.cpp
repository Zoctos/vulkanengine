#include "Framebuffer.h"



Framebuffer::Framebuffer()
{
}


Framebuffer::~Framebuffer()
{
}


void Framebuffer::createFramebuffers(Device *device, SwapChain *swapChain, VkRenderPass renderPass, TextureImage *textureDepth) {

	swapChainFramebuffers.resize(swapChain->getSwapChainImageViews().size());

	//Pour chaque image view on cree un framebuffer
	for (size_t i = 0; i < swapChain->getSwapChainImageViews().size(); i++) {

		//On r�cup�re l'Image view
		std::array<VkImageView, 2> attachments = { swapChain->getSwapChainImageViews().at(i), textureDepth->getTextureView() };

		//On param�tre notre framebuffer
		VkFramebufferCreateInfo framebufferInfo = {};
		framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferInfo.renderPass = renderPass;
		framebufferInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
		framebufferInfo.pAttachments = attachments.data();
		framebufferInfo.width = swapChain->getSwapChainExtent().width;
		framebufferInfo.height = swapChain->getSwapChainExtent().height;
		framebufferInfo.layers = 1;

		//On cr�e le framebuffer de l'image view
		if (vkCreateFramebuffer(device->getLogicalDevice(), &framebufferInfo, nullptr, &swapChainFramebuffers[i]) != VK_SUCCESS) {
			throw std::runtime_error("failed to create framebuffer !");
		}

	}

}

void Framebuffer::cleanup(Device *device) {

	for (size_t i = 0; i < swapChainFramebuffers.size(); i++) {
		vkDestroyFramebuffer(device->getLogicalDevice(), swapChainFramebuffers[i], nullptr);
	}

}

std::vector<VkFramebuffer> Framebuffer::getFramebuffers() {
	return swapChainFramebuffers;
}